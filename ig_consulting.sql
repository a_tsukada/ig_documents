SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS aset_relations;
DROP TABLE IF EXISTS event_members;
DROP TABLE IF EXISTS member_points;
DROP TABLE IF EXISTS likes;
DROP TABLE IF EXISTS shares;
DROP TABLE IF EXISTS topic_comments;
DROP TABLE IF EXISTS topics;
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS asets;
DROP TABLE IF EXISTS community_members;
DROP TABLE IF EXISTS communities;
DROP TABLE IF EXISTS friends;
DROP TABLE IF EXISTS login_logs;
DROP TABLE IF EXISTS receive_notifications;
DROP TABLE IF EXISTS member_devices;
DROP TABLE IF EXISTS questionnaire_answers;
DROP TABLE IF EXISTS questionnaire_answer_manages;
DROP TABLE IF EXISTS questionnaire_questions;
DROP TABLE IF EXISTS questionnaire_manages;
DROP TABLE IF EXISTS requests;
DROP TABLE IF EXISTS send_notifications;
DROP TABLE IF EXISTS members;
DROP TABLE IF EXISTS shops;
DROP TABLE IF EXISTS areas;
DROP TABLE IF EXISTS care_informations;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS devyce_types;
DROP TABLE IF EXISTS friendships;
DROP TABLE IF EXISTS ng_words;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS user_types;




/* Create Tables */

CREATE TABLE areas
(
	id bigint NOT NULL AUTO_INCREMENT,
	no int NOT NULL,
	prefecture varchar(10) NOT NULL,
	name varchar(256) NOT NULL,
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE asets
(
	id bigint NOT NULL AUTO_INCREMENT,
	member_id bigint,
	img_url varchar(256),
	movie_url varchar(256),
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int,
	PRIMARY KEY (id)
);


CREATE TABLE aset_relations
(
	id bigint NOT NULL AUTO_INCREMENT,
	aset_id bigint,
	topic_id bigint,
	event_id bigint,
	community_id bigint,
	care_information_id bigint,
	topics_type int(1),
	aset_type bigint,
	main_flg int(1),
	member_id bigint,
	title varchar(256),
	detail varchar(1024),
	public_flg int DEFAULT 1 NOT NULL,
	start_date varchar(10),
	end_date varchar(10),
	display_order int,
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE care_informations
(
	id bigint NOT NULL AUTO_INCREMENT,
	type int,
	category_id bigint,
	title varchar(256),
	detail varchar(1024),
	public_flg int DEFAULT 1 NOT NULL,
	start_date varchar(10),
	end_date varchar(10),
	display_order int,
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE categories
(
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(30),
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE communities
(
	id bigint NOT NULL AUTO_INCREMENT,
	member_id bigint,
	name varchar(256),
	detail varchar(512),
	notice varchar(512),
	restriction int DEFAULT 0,
	public_flg int DEFAULT 1 NOT NULL,
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int,
	PRIMARY KEY (id)
);


CREATE TABLE community_members
(
	id int unsigned NOT NULL AUTO_INCREMENT,
	community_id bigint,
	member_id bigint,
	submit_date datetime,
	status bigint,
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int,
	PRIMARY KEY (id)
);


CREATE TABLE devyce_types
(
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(20),
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE events
(
	id bigint NOT NULL AUTO_INCREMENT,
	shop_id bigint,
	area_id bigint,
	name varchar(256),
	detail varchar(512),
	open_date varchar(10),
	close_date varchar(10),
	display_order int,
	public_flg int DEFAULT 1 NOT NULL,
	point int DEFAULT 0,
	notification_message varchar(128),
	address varchar(256),
	longitude double,
	latitude double,
	last_publicity_datetime datetime,
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE event_members
(
	id int unsigned NOT NULL AUTO_INCREMENT,
	event_id bigint,
	member_id bigint,
	entry_date varchar(10) NOT NULL,
	entry_comment varchar(256),
	point_recieved int,
	event_status varchar(1),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT unq_member_events01 UNIQUE ()
);


CREATE TABLE friends
(
	id bigint NOT NULL AUTO_INCREMENT,
	member_id bigint,
	friendship_id int unsigned,
	flg int,
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE friendships
(
	id int unsigned NOT NULL AUTO_INCREMENT,
	friend_status_flg int(1),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE likes
(
	id bigint NOT NULL AUTO_INCREMENT,
	topic_id bigint,
	topics_type int(1),
	aset_type bigint,
	member_id bigint,
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE login_logs
(
	id bigint NOT NULL AUTO_INCREMENT,
	member_id bigint,
	login_datetime datetime,
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE members
(
	id bigint NOT NULL AUTO_INCREMENT,
	shop_id bigint,
	last_name varchar(256) NOT NULL,
	first_name varchar(256) NOT NULL,
	email varbinary(256) NOT NULL,
	password varchar(256) NOT NULL,
	birth_year int(4),
	birth_month int(2),
	birth_day int(2),
	point int unsigned DEFAULT 0 NOT NULL,
	public_flg_name int DEFAULT 1,
	public_flg_birthday int DEFAULT 0,
	public_flg_email int DEFAULT 0,
	nickname varchar(256),
	sex int(1),
	zipcode varchar(7),
	prefecture varchar(10),
	city_town varchar(256),
	address varchar(256),
	buildings varchar(256),
	menber_type bigint,
	facebook_url varchar(256),
	comment varchar(256),
	approve_flg int DEFAULT 1,
	public_flg_timeline int,
	public_flg_event int,
	public_flg_community int,
	public_flg_spot int,
	public_flg_area int,
	public_flg_user_kbn int,
	public_flg_facebook int,
	public_flg_sex int,
	home_status int(1),
	career varchar(256),
	annual_income varchar(1),
	roll int,
	img_url varchar(256),
	area_id bigint,
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (email)
);


CREATE TABLE member_devices
(
	id bigint NOT NULL AUTO_INCREMENT,
	member_id bigint,
	device_type bigint,
	device_token varchar(128),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE member_points
(
	id bigint NOT NULL AUTO_INCREMENT,
	member_id bigint,
	event_id bigint,
	request_id bigint,
	login_log_id bigint,
	point bigint DEFAULT 0 NOT NULL,
	action varchar(256),
	remarks varchar(256),
	addition_subtraction_flg int(1),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE ng_words
(
	id bigint NOT NULL AUTO_INCREMENT,
	word varchar(256),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE products
(
	id int unsigned NOT NULL AUTO_INCREMENT,
	name varchar(256) NOT NULL,
	detail varchar(512) NOT NULL,
	picture_url varchar(256),
	picture_url2 varchar(256),
	picture_url3 varchar(256),
	picture_url4 varchar(256),
	picture_url5 varchar(256),
	point int unsigned DEFAULT 0,
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE questionnaire_answers
(
	id bigint NOT NULL AUTO_INCREMENT,
	questionnaire_answer_manage_id bigint,
	question_number bigint,
	question_type int(1),
	answer_select int(1),
	answer_free varchar(1024),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE questionnaire_answer_manages
(
	id bigint NOT NULL AUTO_INCREMENT,
	questionnaire_manage_id bigint,
	member_id bigint,
	answer_date varchar(10),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE questionnaire_manages
(
	id bigint NOT NULL AUTO_INCREMENT,
	shop_id bigint,
	member_id bigint,
	del_flg int DEFAULT 0 NOT NULL,
	title varchar(256),
	detail varchar(1024),
	open_date varchar(10),
	close_date varchar(10),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE questionnaire_questions
(
	id bigint NOT NULL AUTO_INCREMENT,
	questionnaire_manage_id bigint,
	question_number bigint,
	question_detail varchar(1024),
	question_type int(1),
	answer_select int(1),
	answer_free varchar(1024),
	select1 varchar(256),
	select2 varchar(256),
	select3 varchar(256),
	select4 varchar(256),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE receive_notifications
(
	id bigint NOT NULL AUTO_INCREMENT,
	member_id bigint,
	send_notification_id bigint,
	member_device_id bigint,
	receive_date datetime,
	read_flg bigint,
	read_datetime bigint,
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	PRIMARY KEY (id)
);


CREATE TABLE requests
(
	id bigint NOT NULL AUTO_INCREMENT,
	member_id bigint,
	shop_id bigint,
	product_id int unsigned,
	request_date date,
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int,
	PRIMARY KEY (id)
);


CREATE TABLE send_notifications
(
	id bigint NOT NULL AUTO_INCREMENT,
	send_date datetime,
	member_id bigint,
	notification_type int(1),
	aset_type bigint,
	read_flg int(1),
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE shares
(
	id bigint NOT NULL AUTO_INCREMENT,
	topics_type int(1),
	aset_type bigint,
	member_id bigint,
	topic_id bigint,
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE shops
(
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(256) NOT NULL,
	image_url varchar(256),
	area_id bigint,
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE topics
(
	id bigint NOT NULL AUTO_INCREMENT,
	topics_type int(1),
	community_id bigint,
	event_id bigint,
	aset_type bigint,
	category_id bigint,
	name varchar(256),
	detail varchar(512),
	member_id bigint,
	open_date varchar(10),
	close_date varchar(10),
	display_order int,
	public_flg int DEFAULT 1 NOT NULL,
	point int DEFAULT 0,
	longitude double,
	latitude double,
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE topic_comments
(
	id bigint NOT NULL AUTO_INCREMENT,
	topic_id bigint,
	comment varchar(256),
	member_id bigint,
	add_user varchar(128) NOT NULL,
	add_date datetime NOT NULL,
	upd_user varchar(128),
	upd_date datetime,
	del_flg int DEFAULT 0 NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE user_types
(
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(10),
	add_user varchar(128),
	add_date datetime,
	upd_user varchar(128),
	upd_date datetime,
	PRIMARY KEY (id)
);



/* Create Foreign Keys */

ALTER TABLE events
	ADD FOREIGN KEY (area_id)
	REFERENCES areas (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE members
	ADD FOREIGN KEY (area_id)
	REFERENCES areas (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE shops
	ADD FOREIGN KEY (area_id)
	REFERENCES areas (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE aset_relations
	ADD FOREIGN KEY (aset_id)
	REFERENCES asets (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE aset_relations
	ADD FOREIGN KEY (care_information_id)
	REFERENCES care_informations (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE care_informations
	ADD FOREIGN KEY (category_id)
	REFERENCES categories (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE topics
	ADD FOREIGN KEY (category_id)
	REFERENCES categories (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE community_members
	ADD FOREIGN KEY (community_id)
	REFERENCES communities (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE topics
	ADD FOREIGN KEY (community_id)
	REFERENCES communities (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE member_devices
	ADD FOREIGN KEY (device_type)
	REFERENCES devyce_types (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE aset_relations
	ADD FOREIGN KEY (event_id)
	REFERENCES events (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE event_members
	ADD FOREIGN KEY (event_id)
	REFERENCES events (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE member_points
	ADD FOREIGN KEY (event_id)
	REFERENCES events (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE topics
	ADD FOREIGN KEY (event_id)
	REFERENCES events (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE friends
	ADD FOREIGN KEY (friendship_id)
	REFERENCES friendships (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE member_points
	ADD FOREIGN KEY (login_log_id)
	REFERENCES login_logs (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE asets
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE aset_relations
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE communities
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE community_members
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE event_members
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE friends
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE likes
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE login_logs
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE member_devices
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE questionnaire_answer_manages
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE questionnaire_manages
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE receive_notifications
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE requests
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE send_notifications
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE shares
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE topic_comments
	ADD FOREIGN KEY (member_id)
	REFERENCES members (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE receive_notifications
	ADD FOREIGN KEY (member_device_id)
	REFERENCES member_devices (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE requests
	ADD FOREIGN KEY (product_id)
	REFERENCES products (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE questionnaire_answers
	ADD FOREIGN KEY (questionnaire_answer_manage_id)
	REFERENCES questionnaire_answer_manages (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE questionnaire_answer_manages
	ADD FOREIGN KEY (questionnaire_manage_id)
	REFERENCES questionnaire_manages (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE questionnaire_questions
	ADD FOREIGN KEY (questionnaire_manage_id)
	REFERENCES questionnaire_manages (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE member_points
	ADD FOREIGN KEY (request_id)
	REFERENCES requests (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE receive_notifications
	ADD FOREIGN KEY (send_notification_id)
	REFERENCES send_notifications (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE events
	ADD FOREIGN KEY (shop_id)
	REFERENCES shops (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE members
	ADD FOREIGN KEY (shop_id)
	REFERENCES shops (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE aset_relations
	ADD FOREIGN KEY (topic_id)
	REFERENCES topics (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE likes
	ADD FOREIGN KEY (topic_id)
	REFERENCES topics (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE shares
	ADD FOREIGN KEY (topic_id)
	REFERENCES topics (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE topic_comments
	ADD FOREIGN KEY (topic_id)
	REFERENCES topics (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE aset_relations
	ADD FOREIGN KEY (community_id)
	REFERENCES topic_comments (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE members
	ADD FOREIGN KEY (menber_type)
	REFERENCES user_types (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



